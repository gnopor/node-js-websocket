class WebsocketService {
  #serverUrl;
  #client;
  #handlers = new Map();

  constructor(serverUrl) {
    this.#serverUrl = serverUrl;
  }

  on(type = "", callback = () => {}) {
    this.#handlers.set(type, callback);
  }

  emit({ type, payload }) {
    if (!(type && payload)) {
      throw new Error("Wrong emit body.");
    }

    const msg = JSON.stringify({ type, payload });
    this.#client.send(msg);
  }

  connectWebsocket(onConnect = () => {}) {
    const client = new WebSocket(this.#serverUrl);
    this.#client = client;

    client.addEventListener("open", () => onConnect((args) => this.emit(args)));

    client.addEventListener("message", (event) => {
      try {
        const { type, payload } = JSON.parse(event.data.toString());
        this.#handlers.get(type.toLowerCase())?.(payload, (args) =>
          this.emit(args)
        );
      } catch (error) {
        console.error(error, event.data);
      }
    });

    client.addEventListener("close", (event) => {
      if (event.wasClean) return;

      console.error("Socket Connection closed unexpectedly.");
      setTimeout(() => this.connectWebsocket(onConnect), 1000);
    });

    client.addEventListener("error", () => {
      client.close();
    });
  }
}

// test -------------------
const socketServerUrl = "ws://localhost:8000/";
const id =
  new window.URLSearchParams(window.location.search).get("id") || "user1";

const socketService = new WebsocketService(socketServerUrl);

socketService.connectWebsocket((emit) => {
  const newMessage = { type: "save-client", payload: { id } };
  emit(newMessage);
});

socketService.on("client-saved", (data, emit) => {
  console.log(data);
  const newMsg = { type: "ping", payload: { id } };

  setTimeout(() => emit(newMsg), 1000);
});

socketService.on("pong", (data) => {
  console.log(data);
});
