// const socketServerUrl = "wss://javascript.info/article/websocket/chat/ws";
const socketServerUrl = "ws://localhost:8000/";

function connectWebsocket(serverUrl = "") {
  const ws = new WebSocket(serverUrl);

  ws.addEventListener("open", () => {
    ws.send("my name is jhon"); // same as event.target.send("")
  });

  ws.addEventListener("close", (event) => {
    if (!event.wasClean) {
      console.log("Socket Connection closed unexpectedly.");
      setTimeout(() => connectWebsocket(serverUrl), 1000);
    } else {
      ws.close();
    }
  });

  ws.addEventListener("message", (event) => {
    try {
      const data = JSON.parse(event.data.toString());

      console.log(data);
    } catch (error) {
      console.error(error, event.data);
    }
  });

  ws.addEventListener("error", () => {
    console.error("An error related to the WS occured.");
    ws.close();
  });

  return ws;
}

// test -------------------
const client = connectWebsocket(socketServerUrl);
setInterval(() => {
  const msg = {
    type: "some_type",
    payload: `string or object or array or what ever. ${Date.now()}`,
  };
  client.send(JSON.stringify(msg));
}, 5000);
