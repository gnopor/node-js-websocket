import http from "http";
import ws, { WebSocketServer } from "ws";

const PORT = process.PORT || 8000;

// web server -----------
const httpServer = http.createServer();

httpServer.listen(PORT, () => {
  console.log(`Server up and listening on port ${PORT}`);
});

// Websocket --------------
const chatSocketHandler = new Map();
const clientsMap = new Map();

const wsServer = new WebSocketServer({ noServer: true });
httpServer.on("upgrade", (req, socket, head) => {
  wsServer.handleUpgrade(req, socket, head, (ws) =>
    onSocketConnect(ws, wsServer)
  );
});

function onSocketConnect(client) {
  client.on("message", (msg) => {
    try {
      const { type, payload } = JSON.parse(msg.toString());

      chatSocketHandler.get(type.toLowerCase())?.(payload, client);
    } catch (error) {
      console.error(error);
      client.send({ type: "error", payload: error?.message });
    }
  });

  client.on("close", () => {});
}

const broadcast = (type, payload, targets) => {
  const msg = JSON.stringify({ type, payload });

  for (const id of targets) {
    const client = clientsMap.get(id);
    client?.readyState === ws.OPEN && client.send(msg);
  }
};

chatSocketHandler.set("save-client", (data, client) => {
  clientsMap.set(data.id, client);

  const targets = [data.id];
  const payload = { message: "Client has been saved", ...data };
  const type = "client-saved";
  broadcast(type, payload, targets);
});

chatSocketHandler.set("ping", (data) => {
  const { payload, targets = [] } = {
    payload: { message: "yo", ...data },
    targets: ["user1", "user2"],
  };
  const type = "pong";
  broadcast(type, payload, targets);
});
