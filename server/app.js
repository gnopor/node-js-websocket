import http from "http";
import { WebSocketServer, OPEN } from "ws";

const PORT = process.PORT || 8000;

// web server -----------
const server = http.createServer();

server.on("upgrade", (req, socket, head) => {
  wsServer.handleUpgrade(req, socket, head, (ws) =>
    onSocketConnect(ws, wsServer)
  );
});

server.listen(PORT, () => {
  console.log(`Server up and listening on port ${PORT}`);
});

// Websocket --------------
const wsServer = new WebSocketServer({ noServer: true });

function onSocketConnect(client, wsServer) {
  const broadCast = (msg) => {
    for (let client of wsServer.clients) {
      client.readyState === OPEN && client.send(msg);
    }
  };

  client.on("message", (msg) => {
    try {
      const data = JSON.parse(msg.toString());

      const newMsg = JSON.stringify({ type: data.type, payload: data.payload });

      data.type.length && broadCast(newMsg);
    } catch {
      client.send(`WTH is this? ${msg.toString()}`);
    }
  });

  client.on("close", () => {});
}
